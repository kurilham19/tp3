import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;


class Node{
    ArrayList<Node> subdaerah = new ArrayList<>();
    String id;
    Node superdaerah;
    int calonA=0,calonB=0,persenA,persenB,selisih;
    double total;
    
    public Node(String id){
        this.id = id;
    }
    public void tambah_subdaerah(Node d){
        subdaerah.add(d);
    }

    public void set_superdaerah(Node d){
        superdaerah = d;
    }
    
    public void tambah_calonA(int n){
        calonA+=n;
        total+=n;
        selisih=Math.abs(calonA-calonB);
        persenA=(int) Math.floor(calonA*100/total);
        persenB=(int) Math.floor(calonB*100/total);
        //jumlah_suara[0]+=n;
    }

    public void tambah_calonB(int n){
        calonB+=n;
        total+=n;
        selisih=Math.abs(calonA-calonB);
        persenA=(int) Math.floor(calonA*100/total);
        persenB=(int) Math.floor(calonB*100/total);
        //jumlah_suara[1]+=n;
    }
    public void anulir_calonA(int n){
        calonA-=n;
        total-=n;
        selisih=Math.abs(calonA-calonB);
        persenA=(int) Math.floor(calonA*100/total);
        persenB=(int) Math.floor(calonB*100/total);
        //jumlah_suara[0]-=n;
    }

    public void anulir_calonB(int n){
        calonB-=n;
        total-=n;
        selisih=Math.abs(calonA-calonB);
        persenA=(int) Math.floor(calonA*100/total);
        persenB=(int) Math.floor(calonB*100/total);
        //jumlah_suara[1]-=n;
    }
}
public class bismillah{
    static Map<String,Node> semua_daerah = new HashMap<>();
    static Node negara;
    static int[] banyak_pemenang = new int[2];
    public static void main(String[] args) throws IOException{
        banyak_pemenang[0]=0;
        banyak_pemenang[1]=0;
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter output = new BufferedWriter(new OutputStreamWriter(System.out));
        String[] list_calon = input.readLine().split(" ");
        String calonA = list_calon[0];
        String calonB = list_calon[1];
        int jumlah_daerah = Integer.parseInt(input.readLine());
        //System.out.println();
        for (int i=0; i<jumlah_daerah;i++){
            String[] list_wilayah = input.readLine().split(" ");
            String nama_daerah = list_wilayah[0];
            if  (i==0){
                negara = new Node(nama_daerah);
                for (int j=2;j<list_wilayah.length;j++){    
                    String nama_subdaerah = list_wilayah[j];
                    //System.out.println("memasukkan "+nama_subdaerah);
                    Node node_subdaerah = new Node(nama_subdaerah);
                    node_subdaerah.set_superdaerah(negara);
                    negara.tambah_subdaerah(node_subdaerah);
                    semua_daerah.put(nama_subdaerah,node_subdaerah);
                }
                continue;
            }
            for (int j=2;j<list_wilayah.length;j++){
                String nama_subdaerah = list_wilayah[j];
                Node node_subdaerah = new Node(nama_subdaerah); 
                semua_daerah.get(nama_daerah).tambah_subdaerah(node_subdaerah);
                semua_daerah.put(nama_subdaerah,node_subdaerah);
                node_subdaerah.set_superdaerah(semua_daerah.get(nama_daerah));
                //node_subdaerah.superdaerah= semua_daerah.get(nama_daerah);
            }
            // System.out.println("nama daerah "+nama_daerah);
            // System.out.print("punya sub daerah ");
            // for (int j=0;j<semua_daerah.get(nama_daerah).subdaerah.size();j++){
            //     System.out.print(semua_daerah.get(nama_daerah).subdaerah.get(j).id+", ");
            // }
            // System.out.println();
        } 
        int banyak_operasi = Integer.parseInt(input.readLine());
        for (int i=0; i<banyak_operasi;i++){
            String[] operasi = input.readLine().split(" ");
            if (operasi[0].equals("TAMBAH")){
                //System.out.println("t");
                String nama_daerah = operasi[1];
                int jumlahA=Integer.parseInt(operasi[2]);
                int jumlahB=Integer.parseInt(operasi[3]);
                Node temp = semua_daerah.get(nama_daerah);
                while (temp!=null){
                    int jumlahA_sebelum = temp.calonA;
                    int jumlahB_sebelum = temp.calonB;

                    temp.tambah_calonA(jumlahA);
                    temp.tambah_calonB(jumlahB);
                    
                    if (jumlahA_sebelum==jumlahB_sebelum){
                        if (temp.calonA>temp.calonB)
                            banyak_pemenang[0]++;
                        else if (temp.calonA<temp.calonB)
                            banyak_pemenang[1]++;
                    }
                    else if (jumlahA_sebelum<jumlahB_sebelum){
                        if (temp.calonA==temp.calonB)
                            banyak_pemenang[1]--;
                        else if(temp.calonA>temp.calonB){
                            banyak_pemenang[0]++;
                            banyak_pemenang[1]--;
                        }
                    }
                    else if (jumlahA_sebelum>jumlahB_sebelum){
                        if (temp.calonA==temp.calonB)
                            banyak_pemenang[0]--;
                        else if(temp.calonA<temp.calonB){
                                banyak_pemenang[0]--;
                                banyak_pemenang[1]++;
                            }
                    }
                    //System.out.println(temp.id+  " A:"+temp.calonA+", B:"+ temp.calonB);
                    temp = temp.superdaerah;
                }
            }
            else if (operasi[0].equals("ANULIR")){
                //System.out.println("a");
                String nama_daerah = operasi[1];
                int jumlahA=Integer.parseInt(operasi[2]);
                int jumlahB=Integer.parseInt(operasi[3]);
                Node temp = semua_daerah.get(nama_daerah);
                while (temp!=null){
                    int jumlahA_sebelum = temp.calonA;
                    int jumlahB_sebelum = temp.calonB;

                    temp.anulir_calonA(jumlahA);
                    temp.anulir_calonB(jumlahB);
                    
                    
                    if (jumlahA_sebelum==jumlahB_sebelum){
                        if (temp.calonA>temp.calonB)
                            banyak_pemenang[0]++;
                        else if (temp.calonA<temp.calonB)
                            banyak_pemenang[1]++;
                    }
                    else if (jumlahA_sebelum<jumlahB_sebelum){
                        if (temp.calonA==temp.calonB)
                            banyak_pemenang[1]--;
                        else if(temp.calonA>temp.calonB){
                            banyak_pemenang[0]++;
                            banyak_pemenang[1]--;
                        }
                    }
                    else if (jumlahA_sebelum>jumlahB_sebelum){
                        if (temp.calonA==temp.calonB)
                            banyak_pemenang[0]--;
                        else if(temp.calonA<temp.calonB){
                                banyak_pemenang[0]--;
                                banyak_pemenang[1]++;
                            }
                    }

                    //System.out.println(temp.id+  " A:"+temp.calonA+", B:"+ temp.calonB);
                    temp = temp.superdaerah;
                }
            }
            else if (operasi[0].equals("CEK_SUARA")){
                //System.out.println("cs");
                String nama_daerah = operasi[1];
                System.out.println(semua_daerah.get(nama_daerah).calonA+" "+semua_daerah.get(nama_daerah).calonB);
            }
            else if (operasi[0].equals("WILAYAH_MENANG")){
                //System.out.println("wm");
                String calon = operasi[1];
                if (calon.equals(calonA)){
                    System.out.println(banyak_pemenang[0]);
                }
                if (calon.equals(calonB)){
                    System.out.println(banyak_pemenang[1]);
                }
            }
            else if (operasi[0].equals("CEK_SUARA_PROVINSI")){
                //System.out.println("csp");
                for (Node n : negara.subdaerah ){
                    System.out.println(n.id+ " "+ n.calonA+" "+n.calonB);
                }
            }
            else if (operasi[0].equals("WILAYAH_MINIMAL")){
                //System.out.println("wm");
                String calon = operasi[1];
                //int index= calon.equals(calonA) ? 0:1;
                int suara = Integer.parseInt(operasi[2]);
                int counter=0;
                for (Map.Entry<String,Node> w:semua_daerah.entrySet()){
                    if (calon.equals(calonA)){
                        if (w.getValue().persenA>suara){
                            counter++;
                        }      
                    }
                    else if (calon.equals(calonB)){
                        if (w.getValue().persenB>suara){
                            counter++;
                        }      
                    }
                }
                System.out.println(counter);
            }
            else if (operasi[0].equals("WILAYAH_SELISIH")){
                //System.out.println("ws");
            }
        }        
    }
}

